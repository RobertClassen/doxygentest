namespace NAMESPACE
{
	using System;
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public class Foo : MonoBehaviour
	{
		#region Fields
		[SerializeField]
		private Bar bar = null;
		#endregion

		#region Properties

		#endregion

		#region Constructors

		#endregion

		#region Methods
		void Update()
		{
			bar.FooBar();
		}
		#endregion
	}
}